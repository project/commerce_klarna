Commerce Klarna module
----------------------------
Provides payment integration with Klarna invoicing service using Drupal Commerce.

The module currently only provides support for Swedish invoices.

The only functionality provided at this point, is the creation of invoices. After they have been created, they have to be administered through Klarna Online.

It is possible to manage invoices from the store using this API, e.g. add articles, change the amount, activate invoices, delete invoices and so on. The goal is to provide every possible feature that is available through the API.


Installation instructions
----------------------------
* Download the API from http://integration.klarna.com/en/download/api-download
* Rename the folder to "klarna", and put it in your libraries folder.
* Go to admin/commerce/config/payment-methods to enable and configure the payment rule.


Dependencies
----------------------------
* Drupal Commerce (http://drupal.org/project/commerce)
* Libraries API (http://drupal.org/project/libraries)


Module development is sponsored by Odd Hill (http://www.oddhill.se) & Meimi (http://www.meimi.se)
